
// Playing Cards
// Alexander Rosas-Cortez

#include <iostream>
#include <conio.h>

using namespace std;

enum class Ranks
{
	two = 2,
	three, 
	four , 
	five, 
	six ,
	seven,
	eight,
	nine,
	ten,
	jack,
	queen,
	king,
	ace,




};
enum class Suits
{
	clubs, 
	diamonds,
	hearts,
	spades
};


struct Card
{
	Ranks Rank;
	Suits Suit;


};

int main()
{

	Card c1;
	c1.Rank = Ranks::Ace;
	c1.Suit = Suits::Spades;

	(void)_getch();
	return 0;
}
